# Oregon Trail - Basic version

This repo contains the famous Oregon Trail game. This version of game is written in FreeBASIC and is based on the game list found in the [May-June 1978 issue of Creative Computing](https://archive.org/details/creativecomputing-1978-05/page/n102/mode/1up?q=interview)